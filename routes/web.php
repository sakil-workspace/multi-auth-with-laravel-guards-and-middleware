<?php


Route::get('/', function () {
    return view('welcome');
});


Route::get('/customers/login','CustomersLoginController@show_login_form')->name('customers.show_login_form');

Route::post('/customers/login','CustomersLoginController@login')->name('customers.login');

Route::get('/customers/logout','CustomersLoginController@logout')->name('customers.logout');

Route::get('/customers/dashboard',function(){
	return view('customers.dashboard');
})->name('customers.dashboard')->middleware('GoToLogin:customer');





Route::get('/admins/login','AdminsLoginController@show_login_form')->name('admins.show_login_form');

Route::post('/admins/login','AdminsLoginController@login')->name('admins.login');

Route::get('/admins/logout','AdminsLoginController@logout')->name('admins.logout');

Route::get('/dashboard',function(){
	return view('admins.dashboard');	
})->name('admins.dashboard')->middleware('GoToLogin:web');
