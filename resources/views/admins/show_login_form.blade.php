<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Customer Login</title>
		<style type="text/css">
			body{
				width: 50%;
				margin: 50px auto;
				text-align: center;
			}
			.container form{
				text-align: center;
				display: block;
			}

		</style>
	</head>
	<body>
		<div class="container">
			<h1>Admin Login</h1> <hr>
			<form method="post" action="{{route('admins.login')}}">
				@csrf
				@method('post')
				<input type="text" name="email" placeholder="Your email please" autofocus> <br><br>
				<input type="password" name="password" placeholder="Your password please"> <br><br>
				<button type="submit">Login</button>

			</form>
		</div>
		
	</body>
</html>