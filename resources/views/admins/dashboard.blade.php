<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>User Dashboard</title>
		<style type="text/css">
			body{
				width: 50%;
				margin: 50px auto;
				padding: 50px;
				background-color: lightblue;
				text-align: center;


			}
		</style>
	</head>
	<body>
		<div class="container">
			<h1>Admin Dashboard</h1>
			<hr>
			<h3>Hello -
			@guest('web')
			{{ 'Guest user' }}
			@endguest
			@auth('web')
			{{ auth()->guard('web')->user()->name .' ('.auth()->guard('web')->user()->email.')' }}
			@endauth
			</h3>
			<a href="{{ route('admins.logout') }}">Logout</a>
		</div>
		
	</body>
</html>