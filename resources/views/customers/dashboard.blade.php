<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Customer Dashboard</title>
		<style type="text/css">
			body{
				width: 50%;
				margin: 50px auto;
				padding: 50px;
				background-color: pink;
				text-align: center;


			}
		</style>
	</head>
	<body>
		<div class="container">
			<h1>Customer Dashboard</h1>
			<hr>
			<h3>Hello -
			@guest('customer')
			{{ 'Guest Customer' }}
			@endguest
			@auth('customer')
			{{ auth()->guard('customer')->user()->name .' ('.auth()->guard('customer')->user()->email.')' }}
			@endauth
			</h3>
			<a href="{{ route('customers.logout') }}">Logout</a>
		</div>
		
	</body>
</html>