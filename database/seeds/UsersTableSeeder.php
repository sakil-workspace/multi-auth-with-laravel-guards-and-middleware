<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {

    	$model = new App\User;
    	$model->name = 'Sakil User '.mt_rand(1,100);
        $model->email = mt_rand(1,100).'sakil@gmail.com';
        $model->password = bcrypt(12345); //12345
        $model->email_verified_at = now();
        $model->save();
        
    }
}
