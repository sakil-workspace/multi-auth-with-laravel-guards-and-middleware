<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class CustomersTableSeeder extends Seeder
{

    public function run(Faker $faker)
    {

    	$model = new App\Customer;
    	$model->name = 'Sakil Customer '.mt_rand(1,100);
    	$model->email = mt_rand(1,100).'sakil@gmail.com';
    	$model->password = bcrypt(12345); //12345
    	$model->email_verified_at = now();
    	$model->save();

        
    }
}
