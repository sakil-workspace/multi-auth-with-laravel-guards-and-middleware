<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class AdminsLoginController extends Controller
{
	public function __construct(){
		$this->middleware('guest:web')->except('logout');
	}
    public function show_login_form(){
    	return view('admins.show_login_form');
    }

    public function login(Request $request){
    	$authenticated_user =  Auth::guard('web')->attempt($request->only('email', 'password'),true);

    	if($authenticated_user){
    		return redirect()->route('admins.dashboard');
    	}

    }

    public function logout(){
    	Auth::guard('web')->logout();
    	return redirect()->route('admins.show_login_form');
    }
}
