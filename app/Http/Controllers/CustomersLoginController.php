<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class CustomersLoginController extends Controller
{
	public function __construct(){
		$this->middleware('guest:customer')->except('logout');
	}
    public function show_login_form(){
    	return view('customers.show_login_form');
    }

    public function login(Request $request){
    	$authenticated_user =  Auth::guard('customer')->attempt($request->only('email', 'password'),true);
    	
    	if($authenticated_user){
    		return redirect()->route('customers.dashboard');
    	}

    }

    public function logout(){
    	Auth::guard('customer')->logout();
    	return redirect()->route('customers.show_login_form');
    }
}
