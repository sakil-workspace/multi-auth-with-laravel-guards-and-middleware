<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectTologinIfNotAuthenticated
{

    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check() == false) {
        	if($guard == 'customer'){
                return redirect()->route('customers.show_login_form');
            }else{
                return redirect()->route('admins.show_login_form');     
            }
        }

        return $next($request);
    }
}
