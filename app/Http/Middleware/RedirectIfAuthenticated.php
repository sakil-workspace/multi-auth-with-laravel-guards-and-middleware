<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if($guard == 'customer'){
                return redirect()->route('customers.dashboard');
            }else{
                return redirect()->route('admins.dashboard');                
            }
        }

        return $next($request);
    }
}
